<?php 
require_once 'pseudoCrypt.php';
require_once 'db.php';

if (isset($_POST['name'])){
    $name = $_POST['name'];
    $date = $_POST['date'].' '.$_POST['time'];
    $notes = $_POST['notes'];
    $statement = $dbh->prepare("INSERT INTO meetings (name, date, notes ) VALUES(?, ?, ?)");
    var_dump($statement);
    $statement->execute([$name, $date, $notes]);
    $meeting_hash = PseudoCrypt::hash($dbh->lastInsertId(), 6);
    header('Location: '.$host.$meeting_hash);
}
if (!isset($meeting_hash)) {
    $meeting_hash = str_replace("/meetme/", "", $_SERVER['REQUEST_URI']);   
    $meeting_hash = preg_replace("#^/#", "", $meeting_hash);
} 
if (isset ($meeting_hash) && trim($meeting_hash)!=""){ 
    if (isset($_POST['delete'])){
        $dbh->query("DELETE FROM participants WHERE ID='{$_POST['id']}'");
    }
    $id = PseudoCrypt::unhash($meeting_hash);
    if (isset($_POST['participant'])){
        $participant = $_POST['participant'];
        $notes = $_POST['notes'];
        $statement = $dbh->prepare("INSERT INTO participants (name, meeting, notes ) VALUES(?, ?, ? )");
        $statement->execute([$participant, $id, $notes]);
    }
    $result = $dbh->query("SELECT * FROM meetings WHERE ID=$id");
    while ($row = $result->fetch(PDO::FETCH_ASSOC)){ extract($row);
        $meeting_name = $row['name'];
        $meeting_date = $row['date'];
        $meeting_notes = $row['notes'];
    }
    $result = $dbh->query("SELECT * FROM participants WHERE meeting=$id");
    $participants = Array();
    while ($row = $result->fetch(PDO::FETCH_ASSOC)){ extract($row);
        $participants[] = $row;
    }
}
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=320, initial-scale=1">
        <title></title>
        <style>
            body{
                font-family: sans-serif
            }
            label{
                width: 100px;
                display: inline-block
            }
            input,textarea{
                width: 220px;
                font-family: sans-serif;
                box-sizing: border-box;
                margin-top: 10px;
                line-height: 1.7em;
            }
            button.big{
                width: 100%;
                height: 50px;
            }
            .wrapper{
                width: 320px;
                margin: 0 auto;
            }
            table{
                width: 320px;
                
            }
            th{
                text-align: left
            }
            #url{
                width: 100%;
                box-sizing: border-box;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
<?php
if (isset ($meeting_hash) && trim($meeting_hash)!=""){ 
?>
                <a href=".">create new meeting</a>
                <h1><?=$meeting_name?></h1>
                <h3><?=$meeting_date?></h3>
                <p><?=$meeting_notes?></p>
                <!-- <input id=url value="<?=$host.$meeting_hash?>" readonly/> <br><br> -->
                <?php if (count($participants)) { ?>
                <table>
                    <thead>
                        <tr>
                            <th>participants</th>
                            <th>notes</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($participants as $p){ ?>
                        <tr>
                            
                                <td><?=$p['name']?></td>
                                <td><?=$p['notes']?></td>
                                <td><form method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
                                        <input type="hidden" name="id" value="<?=$p['id']?>"/>
                                        <button name="delete">x</button>
                                    </form>
                                </td>
                        </tr>
                        <?php }} else { ?>
                            <h3>No participants yet</h3>
                        <?php } ?>

                    </tbody>
                </table>
                <br>
            <form method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
                <label for="name">my name: </label><input type="text" id="name" name="participant"><br>
                <label for="notes">notes: </label><input id="notes" name="notes"></textarea><br><br>
                <button class="big">add me!</button>
            </form>
            <h4>To invite someone share this link: </h4>
                <p><a href="<?=$host.$meeting_hash?>"><?=$host.$meeting_hash?></a></p>
        </div>
<?php } else { ?>
        <form method="POST" action=".">
            <h1>Create a meeting</h1>
            <label for="name">name it: </label><input type="text" id="name" name="name"><br>
            <label for="date">date:</label><input type="date" id="date" name="date" required><br>
            <label for="date">time:</label><input type="time" id="time" name="time" required><br>
            <label for="notes">notes: </label><textarea id="notes" name="notes"></textarea><br><br>
            <button class="big">create meeting</button>
        </form>
<?php } ?>
    </body>
</html>