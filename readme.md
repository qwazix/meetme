# meetme: a simple meeting coordinator

## How to set up 

### Set up apache to allow `.htaccess` files. 

Add this snippet in your website configuration

```
    <Location />
        Order allow,deny
	    AllowOverride All
        Allow from all
    </Location>
```

### Prepare your database

    create database meetme;
    create user 'meetme'@'localhost' identified by 'somesecret';
    grant all privileges on meetme.* to 'meetme'@'localhost';

### Clone this repo into your webserver document root

    git clone https://gitlab.com/qwazix/meetme 

### Configure the database

Copy `db-sample.php` to `db.php` and set the database password in it. 
