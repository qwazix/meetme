<?php
$tz = 'Europe/Athens';
date_default_timezone_set($tz);
$dbh = new PDO('mysql:host=localhost;dbname=meetme', "meetme", "");
$dbh->query("CREATE TABLE IF NOT EXISTS meetings (id INT PRIMARY KEY AUTO_INCREMENT, name varchar(80), notes varchar(80), date DATETIME DEFAULT CURRENT_TIMESTAMP)");
$dbh->query("CREATE TABLE IF NOT EXISTS participants (id INT PRIMARY KEY AUTO_INCREMENT, name varchar(80), notes varchar(80), meeting INT)");
$dbh->query("SET NAMES utf8");
$dbh->query("SET time_zone = '$tz';");
$host = "http://localhost/meetme/";